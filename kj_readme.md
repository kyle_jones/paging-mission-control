# GitLab Review: Telemetry Data Processing Python Script
The provided Python script reads telemetry data from an input file, processes the data, and generates alert messages based on specific thresholds. Below is an overview of how the script works, followed by suggestions for improvement.

## Overview
The script imports necessary libraries and defines the process_telemetry_data function.
The function reads the input file line by line, parsing and converting each field.
It checks whether the raw value exceeds or falls below predefined thresholds to determine the severity of the alert message.
If an alert message is needed, the function appends it to the alert_messages list.
The main block of the script takes a command-line argument as the input file path, calls the process_telemetry_data function, and prints the alert messages in JSON format.


## Scenario: Processing Telemetry Data with the Python Script
In this scenario, we'll demonstrate how to use the telemetry data processing Python script to analyze satellite telemetry data and generate alert messages.

## Prerequisites
1) Python installed on your local machine.
2) The telemetry data processing script `process_telemetry.py` and a sample telemetry data input file `telemetry_data.txt` saved on your local machine.

### Steps
1) Prepare the telemetry data input file: Create a text file with telemetry data using the following format, separated by |:
`timestamp|satellite_id|red_high|yellow_high|yellow_low|red_low|raw_value|component`
    
    Example data: `20230407 12:30:00.123|1|100.0|80.0|20.0|10.0|95.0|temperature
                  20230407 12:31:00.456|2|50.0|40.0|10.0|5.0|45.0|pressure`


2) Run the script: Open a terminal, navigate to the directory containing the process_telemetry.py script and the telemetry_data.txt file, and execute the script using the following command:

`python process_telemetry.py telemetry_data.txt`

3) Review the output: The script will process the telemetry data and generate alert messages based on the predefined thresholds. The output will be printed in JSON format. In this example, the output should look like this:

```javascript
[
    {
        "satelliteId": 1,
        "severity": "YELLOW HIGH",
        "component": "temperature",
        "timestamp": "2023-04-07T12:30:00.123Z"
    },
    {
        "satelliteId": 2,
        "severity": "YELLOW HIGH",
        "component": "pressure",
        "timestamp": "2023-04-07T12:31:00.456Z"
    }
]
```
