import sys
import json
from datetime import datetime


def process_telemetry_data(input_file):
    alert_messages = []

    with open(input_file, 'r') as file:
        for line in file:
            (timestamp, satellite_id, red_high, yellow_high, yellow_low, red_low, raw_value,
             component) = line.strip().split('|')

            # Convert values to appropriate data types
            timestamp = datetime.strptime(timestamp, "%Y%m%d %H:%M:%S.%f").strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "Z"
            satellite_id = int(satellite_id)
            red_high = float(red_high)
            yellow_high = float(yellow_high)
            yellow_low = float(yellow_low)
            red_low = float(red_low)
            raw_value = float(raw_value)

            severity = None
            if raw_value >= red_high:
                severity = "RED HIGH"
            elif raw_value >= yellow_high:
                severity = "YELLOW HIGH"
            elif raw_value <= red_low:
                severity = "RED LOW"
            elif raw_value <= yellow_low:
                severity = "YELLOW LOW"

            if severity:
                alert_messages.append({
                    "satelliteId": satellite_id,
                    "severity": severity,
                    "component": component,
                    "timestamp": timestamp
                })

    return alert_messages


if __name__ == "__main__":
    input_file = sys.argv[1]  # Replace with the input file path
    alert_messages = process_telemetry_data(input_file)

    # Print the alert messages in JSON format
    print(json.dumps(alert_messages, indent=4))
